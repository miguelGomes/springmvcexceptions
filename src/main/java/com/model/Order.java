package com.model;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public final class Order {

    private final String orderId;

    public Order(Order.Builder builder){
        this.orderId = builder.orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public static class Builder {

        private String orderId;

        public Builder setOrderId(String orderId) {
            this.orderId = orderId;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }

}
