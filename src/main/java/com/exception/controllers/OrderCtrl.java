package com.exception.controllers;

import com.exception.exceptions.OrderNotFoundException;
import com.exception.exceptions.info.ErrorInfo;
import com.model.Order;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by MiguelGomes on 7/16/16.
 */
@RestController
public class OrderCtrl {

    private Map<Integer, Order> orderMap = new HashMap<>();

    {
        Order o = new Order.Builder()
                .setOrderId("1")
                .build();
        orderMap.put(new Integer(o.getOrderId()), o);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public Order getOrder(@PathVariable("id") String id){

        Integer orderId = new Integer(id);

        Order order = orderMap.get(orderId);

        if (order == null) {
            throw new OrderNotFoundException();
        }

        return order;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Number Format Exception")
    @ExceptionHandler(NumberFormatException.class)
    public @ResponseBody ErrorInfo convertionFailed(HttpServletRequest httpServletRequest, Exception ex) {

        return new ErrorInfo(httpServletRequest.getRequestURL().toString(), ex.toString());
    }

}
