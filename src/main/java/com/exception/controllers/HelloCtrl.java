package com.exception.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by MiguelGomes on 7/16/16.
 */
@RestController
public class HelloCtrl {

    @RequestMapping("/hello")
    public String hello() {
        return "Hello World Exception";
    }

}
