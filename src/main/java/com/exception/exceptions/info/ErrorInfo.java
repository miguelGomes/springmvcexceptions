package com.exception.exceptions.info;

/**
 * Created by MiguelGomes on 7/16/16.
 */
public class ErrorInfo {

    private final String url;
    private final String ex;

    public ErrorInfo(String url, String ex) {
        this.url = url;
        this.ex = ex;
    }
}
