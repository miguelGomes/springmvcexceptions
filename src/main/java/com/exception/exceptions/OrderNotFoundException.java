package com.exception.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by MiguelGomes on 7/16/16.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Order not exist")
public class OrderNotFoundException extends RuntimeException {
}
